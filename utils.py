import datetime

from classes import Semester
from tables import weekdays_as_int

# 183 day is about one semester

winter_semester_start_day = 1
winter_semester_start_month = 9
winter_semester_end_day = 31
winter_semester_end_month = 1
summer_semester_start_day = 1
summer_semester_start_month = 2
summer_semester_end_day = 30
summer_semester_end_month = 6


def get_start_time(t):
    if int(t.month) >= 9 or int(t.month) == 1:  # winter semester
        return ['w',datetime.datetime(day=1,month=9,year=int(t.year))]
    else:
        return ['s',datetime.datetime(day=1,month=2,year=int(t.year))]


def get_number_of_semesters(t0, t1):
    starting_sem = get_start_time(t0)
    days = (t1 - t0).days
    current_date = t0
    sem_count = 0
    for i in range(days):
        current_date += datetime.timedelta(days=i)
        if current_date == datetime.datetime(day=winter_semester_start_day, month=winter_semester_start_month,
                                             year=current_date.year):
            sem_count += 1
        if current_date == datetime.datetime(day=summer_semester_start_day, month=summer_semester_end_month,
                                             year=current_date.year):
            sem_count += 1
            # we may lose first semester if t0 > 1.09
    if datetime.datetime(day=winter_semester_start_day, month=winter_semester_start_month,
                         year=t0.year) < t0 < datetime.datetime(day=winter_semester_end_day,
                                                                month=winter_semester_end_month, year=int(t0.year) + 1):
        sem_count += 1
    if datetime.datetime(day=summer_semester_start_day, month=summer_semester_start_month,
                         year=t0.year) < t0 < datetime.datetime(day=summer_semester_end_day,
                                                                month=summer_semester_end_month, year=t0.year):
        sem_count += 1

    return sem_count


def generate_starting_with_winter(t0, t1, limit, current_sem):
    semesters = []
    year = int(t0.year)
    current = current_sem
    for i in range(limit):
        if current == 'w':
            start = datetime.datetime(day=winter_semester_start_day,
                                      month=winter_semester_start_month,
                                      year=year)
            end = datetime.datetime(day=winter_semester_end_day,
                                    month=winter_semester_end_month,
                                    year=year + 1)
            semesters.append(Semester(start, end))
            year += 1
            current = 's'
        else:
            start = datetime.datetime(day=summer_semester_start_day,
                                      month=summer_semester_start_month,
                                      year=year)
            end = datetime.datetime(day=summer_semester_end_day,
                                    month=summer_semester_end_month,
                                    year=year)
            semesters.append(Semester(start, end))
            current = 'w'
    #semesters[0].start_date = t0
    #semesters[len(semesters)-1].end_date = t1
    return semesters


def get_semesters(t0, t1):
    num_of_semesters = get_number_of_semesters(t0, t1)
    starting_sem, first_date = get_start_time(t0)
    if starting_sem == 'w':
        semesters = generate_starting_with_winter(t0, t1, num_of_semesters, 'w')
    else:
        semesters = generate_starting_with_winter(t0, t1, num_of_semesters, 's')

    return semesters


def get_weekly_dates_for_semester(semester, day_as_int, t0, t1):
    weekday = ''
    for x in range(7):
        if semester.start_date is not None:
            if (semester.start_date+datetime.timedelta(days=x)).weekday() == day_as_int:
                weekday = semester.start_date+datetime.timedelta(days=x)
                break
        else:
            if (semester.start+datetime.timedelta(days=x)).weekday() == day_as_int:
                weekday = semester.start+datetime.timedelta(days=x)
    end = semester.end
    #print semester.start, semester.end
    dates = []
    i = 0
    while weekday < end:
        if t0.date() <= weekday.date() < t1.date():
            dates.append(weekday)
            #print weekday
            weekday = weekday + datetime.timedelta(days=7)
            i += 1
        else:
            weekday = weekday + datetime.timedelta(days=7)
            i += 1
    dates.pop()
    #print weekday.weekday(), day_as_int
    return dates


def grab_old_pesels(collection):
    pesels = {}
    for person in collection:
        pesels[person.pesel] = 1
    return pesels
