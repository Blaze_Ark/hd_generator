import datetime


def convert_times(data):
    t0 = datetime.datetime(year=data['t0']['year'], month=data['t0']['month'], day=data['t0']['day'])
    t1 = datetime.datetime(year=data['t1']['year'], month=data['t1']['month'], day=data['t1']['day'])
    t2 = datetime.datetime(year=data['t2']['year'], month=data['t2']['month'], day=data['t2']['day'])
    return [t0, t1, t2]


def convert_date_to_pesel_6(date):
    month = int(date.month) + 20 if int(date.year) >= 2000 else int(date.month)
    month = str(month) if int(month) > 9 else "0{}".format(int(month))
    day = str(date.day) if int(date.day) > 9 else "0{}".format(int(date.day))
    return "{}{}{}".format(str(date.year)[-2:], month, day)
