# -*- coding: utf-8 -*-
import hashlib


class Person:
    def __init__(self, name, surname, pesel, sex):
        self.name = name
        self.surname = surname
        self.pesel = pesel
        self.sex = sex

    def log(self):
        return u"{}|{}|{}".format(self.pesel, self.name, self.surname)

    def generate_inserts(self, table_name):
        return u"insert into {} values ({},{},{})".format(table_name,self.pesel, self.name, self.surname)


class ClassType: # Zajęcia
    def __init__(self, pk, name, class_type, level):
        self.pk = pk
        self.name = name
        self.class_type = class_type
        self.level = level

    def log(self):
        return u"{}|{}|{}|{}".format(self.pk, self.name, self.class_type, self.level)

    def generate_key(self):
        return hashlib.md5("{}{}".format(self.name, self.level).encode('utf-8')).hexdigest()

    def generate_inserts(self, table_name):
        return u"insert into {} values ({},{},{},{})".format(
            table_name, self.pk, self.name, self.class_type, self.level)


class ClassDate: # Terminy
    def __init__(self, pk, pk_class_type, weekday, starting_time, length):
        self.pk = pk
        self.pk_class_type = pk_class_type
        self.weekday = weekday
        self.starting_time = starting_time
        self.length = length
        self.semester = None

    def log(self):
        return u"{}|{}|{}|{}|{}".format(self.pk, self.pk_class_type, self.weekday, self.starting_time, self.length)


class ClassDateTeachers:
    def __init__(self, pk_teacher, pk_class_date):
        self.pk_teacher = pk_teacher
        self.pk_class_date = pk_class_date

        self.person_obj = None
        self.class_obj = None

    def log(self):
        return u"{}|{}".format(self.pk_class_date, self.pk_teacher)


# nieużywane tożsame jako klasa z classdateteachers
class ClassDateParticipants: # implementacja zapisani
    def __init__(self, pk_participants, pk_class_date):
        self.pk_participants = pk_participants
        self.pk_class_date = pk_class_date

    def log(self):
        return u"{} {}".format(self.pk_participants, self.pk_class_date)


class Semester:
    def __init__(self, start, end):
        self.start = start
        self.end = end
        self.start_date = None  # First generation date may be other than semester start
        self.end_date = None  # same as above

    def log(self):
        return u"Starting: {}, ending: {}".format(self.start_date if self.start_date is not None else self.start,
                                                  self.end_date if self.end_date is not None else self.end)


class Presence:
    def __init__(self, pk_class_date, pk_participants, date, presence_flag):
        self.pk_class_date = pk_class_date
        self.pk_participants = pk_participants
        self.date = date
        self.presence_flag = presence_flag

    def log(self):
        return u"{}|{}|{}|{}".format(self.pk_class_date, self.pk_participants, self.date, self.presence_flag)

# todo


class Grade:
    def __init__(self, pk, pk_class_type, pk_participants, pk_teacher, value):
        self.pk = pk
        self.pk_class_type = pk_class_type
        self.pk_participants = pk_participants
        self.pk_teacher = pk_teacher
        self.value = value

    def log(self):
        return u"{}|{}|{}|{}|{}".format(self.pk, self.pk_class_type, self.pk_participants, self.pk_teacher, self.value)
