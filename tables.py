# -*- coding: utf-8 -*-

male_names = ['Piotr', 'Łukasz', 'Damian', 'Bartosz', 'Grzegorz', 'Janusz', 'Jan', 'Eryk', 'Daniel', 'Fryderyk',
              'Marcin', 'Marek', 'Antoni', 'Kazimierz', 'Paweł', 'Zygmunt', 'Euzebiusz', 'Henryk', 'Jacek', 'Ariel',
              'Krzysztof', 'Franek', 'Bożydar', 'Kamil', 'Gaweł', 'Bożydar', 'Mściwój', 'Miłosz']

female_names = ['Katarzyna', 'Joanna', 'Barbara', 'Judyta', 'Anna', 'Helena', 'Sonia', 'Natalia', 'Bożena', 'Grażyna',
                'Henryka', 'Janina', 'Ada', 'Zuzanna', 'Lena', 'Amelia', 'Maria', 'Oliwia', 'Aleksandra', 'Julia',
                'Edyta', 'Emilia', 'Tamara', 'Tatiana', 'Tina', 'Apollonia', 'Wioletta', 'Żaneta', 'Roxana']

male_surnames = ['Nowak', 'Kowalski', 'Wiśniewski', 'Wójcik', 'Kowalczyk', 'Kamiński', 'Lewandowski', 'Zieliński',
                  'Woźniak', 'Szymański', 'Dąbrowski', 'Kozłowski', 'Jankowski', 'Mazur', 'Wojciechowski', 'Kwiatkowski',
                  'Krawczyk', 'Kaczmarek', 'Piotrowski', 'Zając', 'Wilk', 'Kruk', 'Anielski', 'Janik', 'Dobrosławski',
                 'Strzyżecki', 'Rzepliński', 'Kaczyński', 'Trojanowski']

female_surnames = ['Nowka', 'Kowalska', 'Wiśniewska', 'Wójcik', 'Kowalczyk', 'Kamińska', 'Mazur', 'Król', 'Wieczorek',
                   'Majewska', 'Olszewska', 'Stępień', 'Jaworska', 'Malinowska', 'Adamczyk', 'Witkowska', 'Rutkowska',
                   'Michalak', 'Tomaszewska', 'Wilk', 'Kruk', 'Wrzosowska', 'Jelec', 'Rabaczewska', 'Anielska', 'But',
                   'Trojanowska', 'Racławska', 'Wanicka']

education = ['podstawowe', 'pogimazjalne', 'średnie', 'wyższe']

sex_types = ['m', 'f']

advancements_levels = ['początkujący', 'średnio-zaawansowany', 'zaawansowany', 'profesjonalny']

activities_types = ['sportowe', 'artystyczne']

art_classes = ['malarstwo', 'pisanie wierszy', 'dziennikarstwo', 'rzeźba', 'gra na saksofonie', 'gra na pianinie',
               'zespół muzyczny', 'chór', 'rysunek artystyczny', 'rysunek anatomiczny', 'performance', 'śpiew',
               'gra na gitarze', 'gra na trąbce', 'malarstwo na tkaninie', 'decoupage', 'wytwarzanie biżuterii',
               'ceramika', 'grafika - linoryt', 'grafika - cyfrowa', 'modelowanie 3d']

sports_classes = ['szachy', 'piłka nożna', 'koszykówka', 'hokej', 'piłka ręczna', 'brydż', 'siatkówka', 'golf',
                  'zapasy', 'karate', 'łucznictwo', 'gimnastyka korekcyjna', 'gimnastyka', 'tenis', 'tenis stołowy',
                  'unihokej', 'lekkoatletyka', 'boks', 'judo', 'wrotkarstwo', 'biegi przełajowe', 'jeździectwo',
                  'polo', 'squash', 'poker', 'e-sport', 'bieg z przeszkodami']

starting_hours = ["06", "07", "08", "09", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19"]

starting_minutes = [":00", ":15", ":30", ":45"]

class_duration = [45, 60, 75, 90, 120, 135, 150, 165, 180]

weekdays = ["poniedziałek", "wtorek", "środa", "czwartek", "piątek", "sobota"]

weekdays_as_int = [0, 1, 2, 3, 4, 5, 6]  # values mapped as datetime.weekday()

starting_hours_num = len(starting_hours)

starting_minutes_num = len(starting_minutes)

class_duration_num = len(class_duration)

weekdays_num = 6

male_nums = [x for x in range(10) if x % 2 == 1]

female_nums = [x for x in range(10) if x % 2 == 0]

female_names_num = len(female_names)

female_surnames_num = len(female_surnames)

male_name_num = len(male_names)

male_surnames_num = len(male_surnames)

art_classes_num = len(art_classes)

sports_classes_num = len(sports_classes)

