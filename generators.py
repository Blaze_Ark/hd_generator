# -*- coding: utf-8 -*-
import datetime
import random
import xml.etree.ElementTree as ET

from converters import convert_date_to_pesel_6
from tables import *
from classes import *
from utils import get_weekly_dates_for_semester

year_floor = 1930

year_ceiling = int(datetime.date.today().year) - 8

year_ceiling_adult = int(datetime.date.today().year) - 20


def get_sport_name():
    return sports_classes[random.randint(0, sports_classes_num-1)]


def get_art_name():
    return art_classes[random.randint(0, art_classes_num-1)]


def get_female_name():
    name = female_names[random.randint(0, female_names_num - 1)]
    surname = female_surnames[random.randint(0, female_surnames_num - 1)]
    return "{} {}".format(name, surname)


def get_male_name():
    name = male_names[random.randint(0, male_name_num - 1)]
    surname = male_surnames[random.randint(0, male_surnames_num - 1)]
    return "{} {}".format(name, surname)


def generate_birth_date(adult):
    valid = False
    while not valid:
        if not adult:
            year = random.randint(year_floor, year_ceiling)
        else:
            year = random.randint(year_floor, year_ceiling_adult)
        month = random.randint(1, 12)
        day = random.randint(1, 31)
        try:
            date = datetime.datetime(year, month, day)
            valid = True
        except ValueError:
            pass
    return date


def pesel_generator(sex, adult):
    birth = generate_birth_date(adult)
    first_6 = convert_date_to_pesel_6(birth)
    sex_num = random.sample(male_nums, 1)[0] if sex == 2 else random.sample(female_nums, 1)[0]
    random_3 = "".join(random.sample([str(i) for i in range(0, 10)], 3))
    random_last = random.sample([i for i in range(0, 10)], 1)[0]
    return "".join([str(first_6), str(random_3), str(sex_num), str(random_last)])


def generate_teachers(limit, pesels=None):
    teachers = []
    pesels = {}
    if pesels is not None:
        pesels = pesels
    for i in range(limit):
        sex = random.randint(1, 2)
        name = get_female_name() if sex == 1 else get_male_name()
        name, surname = name.split(' ')
        pesel = pesel_generator(sex, True)
        pesel_ok = False
        while not pesel_ok:
            if pesel in pesels.keys():
                pesel = pesel_generator(sex, False)
            else:
                pesels[pesel] = 1
                pesel_ok = True
        teachers.append(Person(name, surname, pesel, sex))
    return teachers


def generate_participants(limit, pesels=None):
    participants = []
    pesels = {}
    if pesels is not None:
        pesels = pesels
    for i in range(limit):
        sex = random.randint(1, 2)
        name = get_female_name() if sex == 1 else get_male_name()
        name, surname = name.split(' ')
        pesel = pesel_generator(sex, False)
        pesel_ok = False
        while not pesel_ok:
            if pesel in pesels.keys():
                pesel = pesel_generator(sex, False)
            else:
                pesels[pesel] = 1
                pesel_ok = True
        participants.append(Person(name, surname, pesel, sex))
        # print participants[i].log()
    return participants


# todo add validation to limit : limit < sport_names * levels + art_name * levels
def generate_class_types(limit, old_types=None, id_s=0):
    c_types = {}
    if old_types is not None:
        c_types = old_types
    i = 0
    while i < limit:
        c_type = activities_types[random.randint(0, 1)] # sportowe/artystyczne
        if c_type == activities_types[0]:
            name = get_sport_name()
        else:
            name = get_art_name()
        pk = i + id_s
        level = advancements_levels[random.randint(0, len(advancements_levels)-1)]
        obj = ClassType(pk, name, c_type, level)
        key = obj.generate_key()
        if key not in c_types:
            c_types[key] = obj
            i += 1
        else:
            pass
    return c_types


def generate_class_dates(c_types, limit, id_s=0):
    ids = [z.pk for z in c_types.values()]
    ids_count = len(ids)
    c_dates = []
    for y in range(limit):
        pk_class_type = ids[random.randint(0, ids_count-1)]
        pk = y + id_s
        weekday = weekdays[random.randint(0, weekdays_num-1)]
        starting_time = "{}{}".format(starting_hours[random.randint(0, starting_hours_num-1)],
                                      starting_minutes[random.randint(0, starting_minutes_num-1)])
        length = class_duration[random.randint(0, class_duration_num-1)]
        c_dates.append(ClassDate(pk, pk_class_type, weekday, starting_time, length))
    return c_dates


def generate_class_dates_teachers_relation(c_dates, teachers, limit):
    ids = [i.pk for i in c_dates]
    ids_count = len(ids)
    techers_pesels = [i.pesel for i in teachers]
    pesel_count = len(techers_pesels)
    relations_valid = {}
    relations = []
    i = 0
    while i < limit:
        random_id = random.randint(0, ids_count-1)
        random_pesel = random.randint(0, pesel_count-1)
        obj = ClassDateTeachers(techers_pesels[random_pesel], ids[random_id])
        obj.class_obj = c_dates[random_id]
        obj.person_obj = teachers[random_pesel]
        key = u"{}{}".format(techers_pesels[random_pesel], ids[random_id])
        if key not in relations_valid.keys():
            relations.append(obj)
            i+=1
        # print obj.log()
    return relations


def generate_presence_objs(c_date_participants, semesters, t0, t1):
    c_dates_weekly = {}
    semesters_num = len(semesters)
    presences = []
    for obj in c_date_participants:
        if obj.pk_class_date in c_dates_weekly.keys():
            all_classes_dates = c_dates_weekly[obj.pk_class_date]
            for date in all_classes_dates:
                present = random.randint(0, 1)  # 0 absent, 1 present
                presences.append(Presence(obj.pk_class_date, obj.pk_teacher, date, present))
        else:
            day_as_int = weekdays_as_int[weekdays.index(obj.class_obj.weekday)]
            semester = semesters[random.randint(0, semesters_num-1)]
            all_classes_dates = get_weekly_dates_for_semester(semester, day_as_int,t0, t1)
            c_dates_weekly[obj.pk_class_date] = all_classes_dates
            for date in all_classes_dates:
                present = random.randint(0, 1)  # 0 absent, 1 present
                presences.append(Presence(obj.pk_class_date, obj.pk_teacher, date, present))
    # print len(presences)
    return presences


def generate_grades(c_dates, limit, teachers_dates, participants_dates, id_s=0):
    class_dates_dict = {}
    participants_dict = {}
    teachers_dict = {}
    #  preparations
    for c_date in c_dates:
        class_dates_dict[c_date.pk] = c_date
    for obj in teachers_dates:
        if obj.pk_class_date in teachers_dict.keys():
            teachers_dict[obj.pk_class_date].append(obj.pk_teacher)
        else:
            teachers_dict[obj.pk_class_date] = [obj.pk_teacher]
    for obj in participants_dates:
        if obj.pk_class_date in participants_dict.keys():
            participants_dict[obj.pk_class_date].append(obj.pk_teacher)
        else:
            participants_dict[obj.pk_class_date] = [obj.pk_teacher]
    #print len(teachers_dict)
    #print len(participants_dict)
    #for key, value in teachers_dict.items():
    #    print key, len(value)
    #for i in range(limit):
    c_dates_num = len(c_dates)
    i = 0
    grades = []
    while i < limit:
        try:
            c_date = c_dates[random.randint(0, c_dates_num - 1)]
            pk_participant = participants_dict[c_date.pk][random.randint(0, len(participants_dict[c_date.pk])-1)]
            pk_teacher = teachers_dict[c_date.pk][0]
            pk_c_date = c_date.pk_class_type
            pk = i + id_s
            grade = random.randint(1, 6)
            grades.append(Grade(pk, pk_c_date, pk_participant, pk_teacher, grade))
            #  print id, pk_c_date, pk_participant, pk_teacher, grade
            i += 1
        except KeyError:
            pass
    return grades


def generate_updates(person_collection, limit):
    updates = []
    person_collection_num = len(person_collection)
    for i in range(limit):
        person = person_collection[random.randint(0, person_collection_num - 1)]
        if person.sex == 1:
            surname_update = get_female_name().split(' ')[1]
        else:
            surname_update = get_male_name().split(' ')[1]
        # TODO sformatować update sqlowy, przypisać do listy updates i zwrócić/zapisać do pliku


def generate_xml(limit, c_dates, c_dates_teachers_relations):
    root = ET.Element('ankiety')

    class_dates_dict = {}
    participants_dict = {}
    teachers_dict = {}
    #  preparations
    for c_date in c_dates:
        class_dates_dict[c_date.pk] = c_date
    relations_num = len(c_dates_teachers_relations)
    for i in range(limit):
        relation = c_dates_teachers_relations[random.randint(0, relations_num-1)]
        pk_class_type = class_dates_dict[relation.pk_class_date].pk_class_type
        kurs = ET.SubElement(root, 'kurs')
        id = ET.SubElement(kurs, 'id')
        id.text = str(pk_class_type)
        ocena = ET.SubElement(kurs, 'ocena')
        ocena.text = str(random.randint(1, 6))
        prowadzacy = ET.SubElement(kurs, 'prowadzacy')
        id_prowadzacy = ET.SubElement(prowadzacy, 'id')
        id_prowadzacy.text = str(relation.pk_teacher)
        widza = ET.SubElement(prowadzacy, 'wiedza')
        widza.text = str(random.randint(0,100))
        podejscie = ET.SubElement(prowadzacy, 'podejscie')
        podejscie.text = str(random.randint(0,100))
        organizacja = ET.SubElement(prowadzacy, 'organizacja')
        organizacja.text = str(random.randint(0,100))
        zaangazowanie = ET.SubElement(prowadzacy, 'zaangazowanie')
        zaangazowanie.text = str(random.randint(0,100))
        przygotowanie = ET.SubElement(prowadzacy, 'przygotowanie')
        przygotowanie.text = str(random.randint(0,100))
    _xml = ET.tostring(root)
    with open('ankiety.xml', 'wb') as f:
        f.write(_xml)
