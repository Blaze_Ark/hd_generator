# -*- coding: utf-8 -*-
import csv
import sys
import datetime
import json

from utils import get_semesters, grab_old_pesels
from validators import validate_time_periods, validate_class_types
from converters import convert_times
from generators import generate_teachers, generate_class_types, generate_class_dates, \
    generate_class_dates_teachers_relation, generate_participants, generate_presence_objs, generate_grades, generate_xml

faker_installed = True

try:
    import faker
except ImportError:
    faker_installed = False


def validate_config():
    with open('config.json') as f:
        data = f.read()
        data_json = json.loads(data)
        t0, t1, t2 = convert_times(data_json)
        if not validate_time_periods(t0, t1, t2):
            print 'Invalid dates ! t0 < t1 < t2'
            return False
        if not validate_class_types(data_json['class_types']):
            print 'Too many class types ! Not enough static data to generate.'
            return False
        return True


def load_config():
    f = open('config.json')
    data = json.loads(f.read())
    f.close()
    return data


def load_dates():
    f = open('config.json')
    data = json.loads(f.read())
    t0, t1, t2 = convert_times(data)
    f.close()
    return [t0, t1, t2]


def save_to_file(collection, filename):
    printable = [[obj.log()] for obj in collection]
    with open(filename, 'wb') as f:
        writer = csv.writer(f)
        writer.writerows(printable)


def append_to_file(collection, filename):
    printable = [[obj.log()] for obj in collection]
    with open(filename, 'a') as f:
        writer = csv.writer(f)
        writer.writerows(printable)


def faker_generation():
    print 'Generating with faker : )'


def file_based_generation(config):
    print 'Generating based on files : ('
    t0, t1, t2 = load_dates()
    semesters = get_semesters(t0, t1)
    teachers = generate_teachers(config['teachers_objects'])
    participants = generate_participants(config['participants_objects'])
    c_types = generate_class_types(config['class_types'])
    c_dates = generate_class_dates(c_types, config['class_dates'])
    c_date_teachers = generate_class_dates_teachers_relation(c_dates, teachers, config['teachers_dates_relations'])
    c_date_participants = generate_class_dates_teachers_relation(c_dates, participants,
                                                                 config['participants_dates_relations'])
    presence_obj = generate_presence_objs(c_date_participants, semesters, t0, t1)
    graders = generate_grades(c_dates, config['grades_objects'], c_date_teachers, c_date_participants)

    # save

    save_to_file(teachers, 'prowadzacy.csv')
    save_to_file(participants, 'uczestnicy.csv')
    save_to_file(c_types.itervalues(), 'zajecia.csv')
    save_to_file(c_dates, 'terminy.csv')
    save_to_file(c_date_teachers, 'prowadzacyterminy.csv')
    save_to_file(c_date_participants, 'zapisani.csv')
    save_to_file(presence_obj, 'obecnosci.csv')
    save_to_file(graders, 'oceny.csv')
    # updates

    # t1 , t2

    semesters_new = get_semesters(t1, t2)
    old_pesels = grab_old_pesels(teachers)
    teachers_n = generate_teachers(config['teachers_objects_new'], old_pesels)
    old_pesels = grab_old_pesels(participants)
    participants_n = generate_participants(config['participants_objects_new'], old_pesels)

    c_types_n = generate_class_types(config['class_types_new'], c_types, config['class_types'])

    teachers_n += teachers
    participants += participants

    c_dates_n = generate_class_dates(c_types_n, config['class_dates_new'], int(config['class_dates']))

    c_date_teachers_n = generate_class_dates_teachers_relation(c_dates_n, teachers_n, config['teachers_dates_relations_new'])
    c_date_participants_n = generate_class_dates_teachers_relation(c_dates_n, participants_n,
                                                                 config['participants_dates_relations_new'])
    presence_obj_n = generate_presence_objs(c_date_participants_n, semesters_new, t1, t2)
    graders_n = generate_grades(c_dates_n, config['grades_objects'], c_date_teachers_n, c_date_participants_n,
                                config['grades_objects'])

    # append to file
    append_to_file(teachers_n, 'prowadzacy.csv')
    append_to_file(participants_n, 'uczestnicy.csv')
    append_to_file(c_types_n.itervalues(), 'zajecia.csv')
    append_to_file(c_dates_n, 'terminy.csv')
    append_to_file(c_date_teachers_n, 'prowadzacyterminy.csv')
    append_to_file(c_date_participants_n, 'zapisani.csv')
    append_to_file(presence_obj_n, 'obecnosci.csv')
    append_to_file(graders_n, 'oceny.csv')

    # brak dat więc po prostu generowane na całym przedziale obiektów
    generate_xml(config['xml_objects'], c_dates+c_dates_n, c_date_teachers+c_date_teachers_n)


def main():
    reload(sys)
    sys.setdefaultencoding('utf-8')
    #datetime.datetime(2016, 9, 31)
    if validate_config():
        config = load_config()
        if faker_installed:
            faker_generation()
        else:
            file_based_generation(config)


if __name__ == "__main__":
    main()
