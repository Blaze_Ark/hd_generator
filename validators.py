from tables import art_classes, sports_classes, advancements_levels


def validate_time_periods(t1, t2, t3):
    if t1 < t2 < t3:
        return True
    return False


def validate_class_types(number_of_objects):
    if int(number_of_objects) > (len(art_classes)+len(sports_classes))* len(advancements_levels):
        return False
    return True
